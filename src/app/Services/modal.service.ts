﻿import * as _ from 'underscore';

export class ModalService {
  private modals: any[] = [];
  public isModalOpen = false;

  add(modal: any) {
    // add modal to array of active modals
    this.modals.push(modal);
  }

  remove(id: string) {
    // remove modal from array of active modals
    let modalToRemove = _.findWhere(this.modals, { id: id });
    if(modalToRemove !== undefined ){
      modalToRemove.close();
    }
    this.isModalOpen = false;
    this.modals = _.without(this.modals, modalToRemove);
  }

  open(id: string) {
    // open modal specified by id
    let modal = _.findWhere(this.modals, { id: id });
    this.isModalOpen = true;
    modal.open();
  }

  closeAll(){
   // console.log(this.modals);
    this.modals.forEach((item: any) => {
      let modal = _.find(this.modals, { id: item.id});
      this.isModalOpen = false;
    modal.close();
    });
  }

  close(id: string) {
    // close modal specified by id
    let modal = _.find(this.modals, { id: id });
    if(modal !== undefined ){
      this.isModalOpen = false;
      modal.close();
    }

  }
}
