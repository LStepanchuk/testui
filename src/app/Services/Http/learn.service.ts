import { Http, Response, Headers, RequestOptions , URLSearchParams} from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';

@Injectable()
export class LearnService {
  constructor(
    //    private _http: HttpClient,
    private _http: Http,
    private _connectionService: ConnectionService

  ) { }
  innerUrl = '/api/v1/image/learn';

   UploadImage(file: any): Promise<any> {
    //'/api/v1/image/learn/analyze',
    const form_data: FormData = new FormData();
   // form_data.append( file.name , file);
   // form_data.append('category_id', file.category_id);
    form_data.append('image', file, file.name );
    //    const options = this._connectionService.GetRequestOptions();
    //   options.headers.append('Content-Type', 'multipart/form-data');
    //   console.log(form_data.getAll('file'))
    return new Promise(resolve => {
      this._http.post(this._connectionService.GetServerUrl() + this.innerUrl + '/analyze', form_data, this._connectionService.GetRequestOptions()).subscribe(
        value => { resolve(value); },
        error => { resolve(error); });
    });
  }


  UploadImageWithModel(file: any, model_name: string, model_type: string): Promise<any> {
    //'/api/v1/image/learn/analyze/20181007_cs6/facial',
    const form_data: FormData = new FormData();
    form_data.append('image', file, file.name );
    return new Promise(resolve => {
      this._http.post(this._connectionService.GetServerUrl() + this.innerUrl + '/analyze/'+ model_name + '/' + model_type, form_data, this._connectionService.GetRequestOptions()).subscribe(
        value => { resolve(value); },
        error => { resolve(error); });
    });
  }

  GetLog(): Promise<any> {
    //api/v1/image/learn/log
    return new Promise(resolve => {
      this._http.get(this._connectionService.GetServerUrl() +  this.innerUrl + '/log', this._connectionService.GetRequestOptions()).subscribe(
        value => { resolve(value); },
        error => { resolve(error); });
    });
  }

  GetModelsList(): Promise<any> {
    //api/v1/image/learn/analyze/models
    return new Promise(resolve => {
      this._http.get(this._connectionService.GetServerUrl() +  this.innerUrl + '/analyze/models', this._connectionService.GetRequestOptions()).subscribe(
        value => { resolve(value); },
        error => { resolve(error); });
    });
  }


  UploadImageLink(link: any): Promise<any> {
    let form_data = {url: link};
    return new Promise(resolve => {
      this._http.post(this._connectionService.GetServerUrl() + this.innerUrl + '/analyze', form_data, this._connectionService.GetRequestOptions()).subscribe(
        value => { resolve(value); },
        error => { resolve(error); });
    });
  }

  GetResults(): Promise<any> {
    //api/v1/image/learn
    return new Promise(resolve => {
      this._http.get(this._connectionService.GetServerUrl() +  this.innerUrl, this._connectionService.GetRequestOptions()).subscribe(
        value => { resolve(value); },
        error => { resolve(error); });
    });
  }


  GetStatistic(): Promise<any> {
    //api/v1/image/learn/statistic
    return new Promise(resolve => {
      this._http.get(this._connectionService.GetServerUrl() +  this.innerUrl + '/statistic', this._connectionService.GetRequestOptions()).subscribe(
        value => { resolve(value); },
        error => { resolve(error); });
    });

  }

  Learn(  md5: any, origin: any): Promise<any> {
  // '/api/v1/image/learn', this.md5, res);
    return new Promise(resolve => {
      this._http.post(this._connectionService.GetServerUrl() + this.innerUrl + '/' + md5 + '/'+ origin , {}, this._connectionService.GetRequestOptions()).subscribe(
        value => { resolve(value); },
        error => { resolve(error); });
    });
  }



  DeleteRecord(user_id: string, blog_id: string, record_id: string ): Promise<any> {
    return new Promise(resolve => {
      this._http.delete(this._connectionService.GetServerUrl() +  this.innerUrl + '/' + user_id + '/' + blog_id + '/' + record_id , this._connectionService.GetRequestOptions()).subscribe(
        value => { resolve(value); },
        error => { resolve(error); });
    });
  }



}



