﻿import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';

import { LogInViewModel, RegisterViewModel, ResetUserWithMailViewModel, ResetUserWithTokenViewModel, UserViewModel } from '../../Models/auth.model';

@Injectable()
export class AuthService {
    constructor(
        private _http: Http,
        private _connectionService: ConnectionService

    ) { }



  Login(item: any): Promise<any> {
   ///api/v1/image/auth
    return new Promise(resolve => {
      this._http.post(this._connectionService.GetServerUrl() + '/api/v1/image/auth', item ).subscribe(
        value => { resolve(value); },
        error => { resolve(error); });
    });
  }

  PreRegister(item: any): Promise<any> {
    //.cleversphere.com/preregister \
  //  -d 'email=j.smith@gmail.com' \
    return new Promise(resolve => {
      this._http.post(this._connectionService.GetServerUrl() + '/preregister', item ).subscribe(
        value => { resolve(value); },
        error => { resolve(error); });
    });
  }

  UploadAvatarImageForPreregister(file: any, token: string): Promise<any> {
    const form_data: FormData = new FormData();
    form_data.append('token', token );
    form_data.append('avatar', file, file.name );
    return new Promise(resolve => {
      this._http.post(this._connectionService.GetServerUrl() +  '/register' , form_data, this._connectionService.GetRequestOptions()).subscribe(
          value => { resolve(value); },
          error => { resolve(error); });
    });
  }


  Register(item: RegisterViewModel): Promise<any> {
    /*
     name
     token
     password
     password_confirmation
     */
    return new Promise(resolve => {
      this._http.post(this._connectionService.GetServerUrl() + '/register',  item).subscribe(
          value => { resolve(value); },
          error => { resolve(error); });
    });
  }


  TestPreRegisterToken(item: any): Promise<any> {
    /*
     'token=TTkok...'
    testmode=true'
     */
    return new Promise(resolve => {
      this._http.post(this._connectionService.GetServerUrl() + '/register',  item).subscribe(
          value => { resolve(value); },
          error => { resolve(error); });
    });
  }



  Logout(): Promise<any> {
    return new Promise(resolve => {
      this._http.post(this._connectionService.GetServerUrl() + '/logout', this._connectionService.GetRequestOptions()).subscribe(
        value => { resolve(value); },
        error => { resolve(error); });
    });
  }

  ResetPassword(item: any): Promise<any> {
    return new Promise(resolve => {
      this._http.post(this._connectionService.GetServerUrl() + '/password/email', item, this._connectionService.GetRequestOptions()).subscribe(
        value => { resolve(value); },
        error => { resolve(error); });
    });
  }

  ResetPasswordWithToken(item: any): Promise<any> {
    return new Promise(resolve => {
      this._http.post(this._connectionService.GetServerUrl() + '/password/reset', item, this._connectionService.GetRequestOptions()).subscribe(
        value => { resolve(value); },
        error => { resolve(error); });
    });
  }

}

