﻿import { Http, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient, HttpParams } from '@angular/common/http';
import { SupportService } from './../support.service';
import { AppConfig } from '../../app.config';
import {unescape} from "querystring";

@Injectable()
export class ConnectionService {
  constructor(private _http: Http,
              private appConfig: AppConfig,
              private supportService: SupportService
              ) {
  }

  fileStoreUrl = this.appConfig.getApi('API_ROOT_URL');
  serverRootUrl = this.appConfig.getApi('API_ROOT_URL');



 public AddTokenParamToRequest(data: any){
   let currentToken = this.GetToken();
   if(currentToken!=''){
     data['authtoken'] =  currentToken;
   }
   return data;
}

  public GetRequestOptions(): any {
    let currentToken = this.GetToken();
   let headers = new Headers();
    const params = new URLSearchParams();
    if(currentToken!=''){
      headers = new Headers({'Authorization': 'Bearer ' + this.GetToken()});
      params.set('authtoken', currentToken);
    }

    const options = new RequestOptions({headers: headers});
    options.search = params;
    return options;
  }

  public GetToken(): string {
    const res = localStorage.getItem('token');
    if (res !== undefined) {
      return res;
    } else {
      return '';
    }
  }

  public SetToken(newToken: string) {
    localStorage.setItem('token', newToken);
  }


  public GetRequestOptionsWithParams(new_params: any): any {
    if (new_params != null) {
      const options = this.GetRequestOptions();
      const params = new URLSearchParams();
      Object.keys(new_params).forEach(key => {
       params.set(key, new_params[key]);
      });
      options.search = params;
      return options;
    } else { return this.GetRequestOptions(); }

  }

  public ResponseSuccess(obj: any, key: string, value: any): boolean {
    return obj.hasOwnProperty(key) && obj[key] === value;
  }

  GetFileContent(url: string): Promise<any> {
    let headers: any = {headers: new Headers({'Access-Control-Allow-Origin': '*'})};
    return new Promise(resolve => {
      this._http.get(url , headers).subscribe(
        value => { resolve(value); },
        error => { resolve(error); });
    });
  }

  public SetCurrentUser(user: any) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  public GetCurrentUser() {
    const res = JSON.parse(localStorage.getItem('user'));
    if (res !== undefined && res !== null ) {
      this.supportService.ChangeUserName(res.user);
      return res;
    } else {
      return null;
    }
  }

  public GetFileStoreUrl(): string {
    return this.fileStoreUrl;
  }

  public GetServerUrl() {
    return this.serverRootUrl;
  }

  public ParseServerError(error: any): string {
    console.log('error');
    if (error._body) {
      if(error.status === 0) {
        return 'Check Internet Connection';
      } else {
        const result = JSON.parse(error._body);
        if (result.message !== undefined) {
          return result.message;
        } else  if (result.errors !== undefined) {
          return result.errors;
        } else {
          return 'Error'; }
      }

    } else {
      return 'Error';
    }
  }
}
