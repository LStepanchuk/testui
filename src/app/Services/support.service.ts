﻿import { AbstractControl } from '@angular/forms';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()

export class SupportService {
  constructor() {}

  private userNameSource = new BehaviorSubject<string>('');
  currentUserName = this.userNameSource.asObservable();

  ChangeUserName(username: string) {
    this.userNameSource.next(username);
  }


  ConvertObjToResArray(list: any): any[] {
    let result = Object.keys(list).map(function(personNamedIndex){
      let item  = { name: personNamedIndex, value: list[personNamedIndex]};
      return item;
    });
    return result;
  }



}

