﻿import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ConnectionService } from '../Services/Http/connection.service';
import { Injectable } from '@angular/core';


@Injectable()
export class LoggedInGuard implements CanActivate {
  constructor(
    private router: Router,
    private connectionService: ConnectionService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser =  this.connectionService.GetCurrentUser();
    if ( currentUser !== null && currentUser.id !== undefined &&  currentUser.id !== 0 ) {
      console.log('to db');
      this.router.navigate(['/dashboard']);
      return false;
    }
    return true;
  }
}

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private connectionService: ConnectionService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser =  this.connectionService.GetCurrentUser();
    if (currentUser !== null && currentUser.id !== 0 && currentUser.id !== null) {
      return true;
    } else {
      this.connectionService.SetCurrentUser(null);
      this.router.navigate(['/']);
      return false ;
    }
  }
}
