﻿import { AbstractControl } from '@angular/forms';
import { Injectable } from '@angular/core';

@Injectable()

export class CustomValidatorService {
    constructor(

    ) { }

    public CustomZipValidator(control: AbstractControl) {
        if (control.value != undefined) {
            let tempValue = control.value.replace(/-/g, '').replace(/_/g, '');
            if (tempValue.length == 5 || tempValue.length == 9) {
                return null;
            }
        }
        return { zip: "Zip Code Error" };
    }

  public CustomPhoneValidator(control: AbstractControl) {
    if (control.value !== undefined) {
      const phoneRegex = /^$|^[0-9]{2}?([ -]?)[0-9]{3}?([ -]?)[0-9]{4}$|^([ -]?)[0-9]{3}?([ -]?)[0-9]{3}?([ -]?)[0-9]{4}$|^(\+)?([ -]?)[0-9]{3}?([ -]?)[0-9]{3}?([ -]?)[0-9]{4}$|^([\+]?)972?([ -]?)[0-9]{2}?([ -]?)[0-9]{3}?([ -]?)[0-9]{4}$|^([\+]?)972?([ -]?)[0-9]{1}?([ -]?)[0-9]{7}$/;
     if (phoneRegex.test(control.value)) {
        return null;
      }
    }
    return { phone: 'Incorrect phone number' };
  }

  public CustomNameValidator(control: AbstractControl) {
    if (control.value !== undefined) {
      const nameRegex = /^[A-Za-z\u0590-\u05fe\-]{2,}?([\.\ ]?)[A-Za-z\u0590-\u05fe\-\.]{2,}?([\.\ ]?)([\-\.A-Za-z\u0590-\u05fe]*?)$/i;
      if (nameRegex.test(control.value)) {
        return null;
      }
    }
    return { name: 'Incorrect name' };
  }


  public CustomEmailValidator(control: AbstractControl) {
        if (control.value !== undefined) {
            const regex = /^[^"@\\()\[\]<>.,;:\s]+(\.[^"@\\()\[\]<>.,;:\s]+)*@(([\u00FF-\uFFFF\-\w]+\.)+[\u00FF-\uFFFF\w]{2,})$/g;
            if (regex.test(control.value)) {
                return null;
            }
        }
        return { customEmail: "Email is not valid" };
    }
}
