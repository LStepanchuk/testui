﻿import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';

import { AuthService } from '../../../Services/Http/auth.service';
import { ConnectionService } from '../../../Services/Http/connection.service';
import { AlertService } from '../../../Services/alert.service';
import { CustomValidatorService} from '../../../Services/validation.service';
import { ModalService } from '../../../Services/modal.service';
import { SupportService } from '../../../Services/support.service';


@Component({
    selector: 'sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in.component.css']
})
export class SignInComponent {
  formGroup: FormGroup;
  constructor(
    private router: Router,
    private connectionService: ConnectionService,
    private alertService: AlertService,
    private modalService: ModalService,
    private authService: AuthService,
    private supportService: SupportService
  )  {

  }

  auth = {user: '', pass: '',  name: '', id: 1111}; //tmp id
  currentUser = {user: '', pass: '', name: '', id: 1111}; //tmp id


  OnLogin(){
          this.currentUser = this.auth;
          this.connectionService.SetCurrentUser(this.auth);
          this.supportService.ChangeUserName(this.currentUser.user);
          this.router.navigate(['/dashboard']);

  }


  OnLoginWithHttpService(){
    this.authService.Login(this.auth).then(item => {
      if (this.connectionService.ResponseSuccess(item, 'status', 200)) {
        const result = item.json();
        if(result.fullaccess){
          this.currentUser = this.auth;
          this.connectionService.SetToken(result.authtoken);
          this.connectionService.SetCurrentUser(this.auth);
          this.supportService.ChangeUserName(this.currentUser.user);
          this.router.navigate(['/dashboard']);

        //  this.isLoggedIn = true;
        } else {
          this.alertService.error('Demo mode');
        }
        console.log(result);
      } else {
        this.alertService.error('Login error ');
      }
    });
  }




  onKeyDown($event){
    console.log('enter');
     // this.LoginBtnClick();
  }

}
