﻿import { Router, NavigationEnd, Event } from '@angular/router';
import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { ConnectionService } from '../../../Services/Http/connection.service';
import { AuthService } from '../../../Services/Http/auth.service';

import { AlertService } from '../../../Services/alert.service';
import { ModalService } from '../../../Services/modal.service';
import { SupportService } from '../../../Services/support.service';
import { TranslateService } from '@ngx-translate/core';

import 'rxjs/add/operator/filter';


@Component({
    selector: 'main-menu',
    templateUrl: './main-menu.component.html',
    styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent {
  activeTab = '';
  activeLang = 'en';
  langList =  [];
  isLoggedIn = false;
  currentUser = {user: '', pass: ''};
  user = {};

  constructor(
    private router: Router,
    private connectionService: ConnectionService,
    private alertService: AlertService,
    private modalService: ModalService,
    private authService: AuthService,
    private translate: TranslateService,
    private supportService: SupportService
) {}

  ngOnInit() {
    this.langList = this.translate.getLangs();
    this.currentUser =  this.connectionService.GetCurrentUser();
    if ( this.currentUser !== null && this.currentUser.user !== '' ) {
      this.isLoggedIn = true;
    }
  }

  SwitchLanguage(lang) {
      this.activeLang = lang;
      this.translate.use(this.activeLang);
  }


  OnLogoutClick() {
         this.connectionService.SetToken('');
         this.supportService.ChangeUserName('');
         this.connectionService.SetCurrentUser(null);
         this.currentUser = null;
         this.isLoggedIn = false;


         this.router.navigate(['/']);
  }



  ngOnDestroy() {
   // this.subscription.unsubscribe();
  }

}
