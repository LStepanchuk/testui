﻿import { Component, OnInit } from '@angular/core';
import { Alert, AlertType } from '../../../Models/alert.model';
import { AlertService } from '../../../Services/alert.service';

@Component({
 //   moduleId: module.id,
    selector: 'alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.css']
})


export class AlertComponent {
    alerts: Alert[] = [];

    constructor(private alertService: AlertService) { }

    ngOnInit() {
        this.alertService.getAlert().subscribe((alert: Alert) => {
            if (!alert) {

                this.alerts = [];
                return;
            }
            this.alerts = this.alerts.filter( obj => obj.message !== alert.message);
            this.alerts.push(alert);
          setTimeout(() => this.removeAlert(alert), 2500);
        });
    }

    removeAlert(alert: Alert) {
        this.alerts = this.alerts.filter(x => x !== alert);
    }

    img(alert: Alert) {
        if (!alert) {
            return;
        }

        switch (alert.type) {
            case AlertType.Success:
                return 'ic_message_success.svg';
            case AlertType.Error:
                return 'ic_message_failed.svg';
            case AlertType.Info:
                return 'ic_message_info.svg';
            case AlertType.Warning:
                return 'ic_message_failed.svg';
        }
    }

    cssClass(alert: Alert) {
        if (!alert) {
            return;
        }

        switch (alert.type) {
            case AlertType.Success:
                return 'alert-success';
            case AlertType.Error:
                return 'alert-error';
            case AlertType.Info:
                return 'alert-info'
            case AlertType.Warning:
                return 'alert-warning';
        }
    }
}
