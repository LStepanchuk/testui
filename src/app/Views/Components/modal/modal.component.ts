﻿import { Component, ElementRef, Input, Output, OnInit, OnDestroy, EventEmitter, HostListener } from '@angular/core';
import * as $ from 'jquery';
import { ModalService } from '../../../Services/modal.service';

@Component({
    selector: 'modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.css'],
    host: { 'class': 'ui-full-height' }
})

export class ModalComponent implements OnInit, OnDestroy {
    @Input() id: string;
    @Input() modalTitle = 'Modal';
    @Input() isSmallModal = false;
    private element: any;
    private isDismissed = true;

    constructor(private modalService: ModalService, private el: ElementRef) {
        this.element = $(el.nativeElement);
    }

    ngOnInit(): void {
        let modal = this;
        if (!this.id) {
            console.error('modal must have an id');
            return;
        }

        this.element.appendTo('body');
        this.modalService.add(this);
    }

    ngOnDestroy(): void {
        this.modalService.remove(this.id);
        this.element.remove();
    }

    open(id: string): void {
        this.isDismissed = false;
        this.element.show();
        this.element.focus();
        $('body').addClass('modal-open');
    }

    dismissModal(): void {
        if (!this.isDismissed) {
            this.close();
        }
    }

  closeAll(): void {
    this.isDismissed = true;
    this.element.hide();
    $('body').removeClass('modal-open');
  }

    close(): void {
        this.isDismissed = true;
        this.element.hide();
        $('body').removeClass('modal-open');
    }


    onKeyUp($event: any) {
        this.dismissModal();
    }

}
