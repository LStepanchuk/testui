﻿import { Component, ElementRef, Input, Output, OnInit, OnDestroy, EventEmitter, HostListener } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';



@Component({
    selector: 'validator',
    templateUrl: './validator.component.html',
    styleUrls: ['./validator.component.css']
})
export class ValidatorComponent {

  @Input() formGroup: FormGroup;
  @Input() validationValue: any;
  @Input() formName: string = '';
  @Input() maxRate: number = 3;
  @Input() isShow: boolean = false;

  @Output() outputRate: number = 2;
  constructor(private formBuilder: FormBuilder) {
    this.formGroup = new FormGroup({ });
  }


  ngAfterViewInit() {
 // console.log(this.formGroup.get(this.formName));
  }

}
