﻿import { Component, Input, Output, EventEmitter, AfterViewInit, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute  } from '@angular/router';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Http, Response, Headers, RequestOptions, URLSearchParams  } from '@angular/http';
import { ConnectionService } from '../../../../Services/Http/connection.service';
import { AlertService } from '../../../../Services/alert.service';
import { SupportService } from '../../../../Services/support.service';
import { ModalService } from '../../../../Services/modal.service';
@Component({
  selector: 'main-dashboard',
  templateUrl: './main-dashboard.component.html',
  styleUrls: ['./main-dashboard.component.css']
})

export class MainDashboardComponent {
  showLoader = false;
  activeVideo: any;

  namesList = ['test video1', 'test video2', 'test video3', 'test video4', 'test video567', 'test video711', 'test video1' ];
  imgList = ['/assets/video/img1.jpg', '/assets/video/img3.jpg', '/assets/video/img4.jpg', '/assets/video/img2.jpg', '/assets/video/img5.jpg' ];
  isActiveList = [true, false];

  videoList = [
    {name: 'test video1', id: 1, source: '/assets/video/img1.jpg', type: 'video/webm', isActive: true},
  ];

  constructor(private router: Router,
              private activeRoute: ActivatedRoute,
              private connectionService: ConnectionService,
              private supportService: SupportService,
              private alertService: AlertService,
              private modalService: ModalService,
              private http: Http) {
  }


  onVideoClick(item) {
  this.activeVideo = item;
  //todo: get random inf
  }

  ngOnDestroy() {
     this.modalService.closeAll();
  }

  ngOnInit() {
    this.OnGetRandomDataForCameras();
    /*  const self = this;
      setInterval(function() {
         self.OnGetRandomDataForCameras();
      },
      5000);*/
  }


  OnGetRandomDataForCameras(){
    this.videoList =  Array.from({length: this.GetRandNumber(15)}, () => this.GetRadonItem());
  }

  GetRandNumber(max) {
    return 1 + Math.floor(Math.random() * max);
  }

  GetRadonItem() {
    return {
      name: 'test video ' + this.GetRandNumber(100) ,
      id: this.GetRandNumber(100),
      source: this.GetRandElement(this.imgList),
      type: 'video/webm',
      isActive: this.GetRandElement(this.isActiveList)};
  }

  GetRandElement(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
  }

}
