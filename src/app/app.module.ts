import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppConfig,  ConfigModule } from './app.config';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HttpModule} from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { DefaultComponent } from './Views/default.component';
import { MainMenuComponent } from './Views/Components/main-menu/main-menu.component';

import { DashboardComponent } from './Views/Dashboard/dashboard.component';
import { MainDashboardComponent } from './Views/Dashboard/Content/main-dashboard/main-dashboard.component';
import { SettingsComponent } from './Views/Dashboard/Content/settings/settings.component';
import { ReportingComponent } from './Views/Dashboard/Content/reporting/reporting.component';

import { AccountComponent } from './Views/Account/account/account.component';
import { SignInComponent } from './Views/Account/sign-in/sign-in.component';

import { AlertComponent } from './Views/Components/alert/alert.component';
import { ValidatorComponent} from './Views/Components/validator/validator.component';
import { LoaderComponent} from './Views/Components/loader/loader.component';
import { ModalComponent } from './Views/Components/modal/modal.component';
import { ErrorComponent } from './Views/error.component';

import { ConnectionService } from './Services/Http/connection.service';
import { AuthService } from './Services/Http/auth.service';
import { LearnService } from './Services/Http/learn.service';
import { SafePipe} from './Services/safe-content';
import { AlertService } from './Services/alert.service';
import { ModalService } from './Services/modal.service';
import { SupportService } from './Services/support.service';
import { CustomValidatorService} from './Services/validation.service';
import { LoggedInGuard, AuthGuard } from './Services/route-guard.service';


@NgModule({
  declarations: [
    AppComponent,
    DefaultComponent,
    AlertComponent,
    AccountComponent,
    SignInComponent,
    DashboardComponent,
    MainDashboardComponent,
    MainMenuComponent,
    ModalComponent,
    ReportingComponent,
    SettingsComponent,
    ErrorComponent,
    ValidatorComponent,
    LoaderComponent,
    SafePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    AppConfig,
    ConfigModule.init(),
    AlertService,
    ModalService,
    ConnectionService,
    CustomValidatorService,
    AuthService,
    LearnService,
    SupportService,
    LoggedInGuard,
    AuthGuard,
    { provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}



