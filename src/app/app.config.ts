import { Injectable, APP_INITIALIZER } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { environment } from '../environments/environment';

@Injectable()
export class AppConfig {

  private _config: Object
  private _env: string;

  constructor(private _http: Http) { }
  load() {
    return new Promise((resolve, reject) => {
      this._env = 'development';
      if (environment.production)
        this._env = 'production';
      console.log(this._env)
      this._http.get('./assets/config/' + this._env + '.json')
        .map(res => res.json())
        .subscribe((data) => {
            this._config = data;
            resolve(true);
          },
          (error: any) => {
            console.error(error);
            return Observable.throw(error.json().error || 'Server error');
          });
    });
  }

  isDevmode() {
    return this._env === 'development';
  }

  getApi(key: string): string {
    return this._config['API_ENDPOINTS'][key];
  }
  get(key: any) {
    return this._config[key];
  }
}

export function ConfigFactory(config: AppConfig) {
  return () => config.load();
}

export function init() {
  return {
    provide: APP_INITIALIZER,
    useFactory: ConfigFactory,
    deps: [AppConfig],
    multi: true
  }
}

const ConfigModule = {
  init: init
}

export { ConfigModule };
