
export class LogInViewModel {
  email: string;
  password: string;
}

export class UserViewModel {
    id: number;
    name: string;
    email: string;
    created_at: string;
    updated_at: string;
}


export class RegisterViewModel {
  name: string;
  email: string;
  password: string;
  password_confirmation: string;
}


export class ResetUserWithMailViewModel {
  email: string;
}

export class ResetUserWithTokenViewModel {
    email: string;
    password: string;
    password_confirmation: string;
    token: string;
}

