export class ProgressBarItem {
  title: string;
  color: string;
  value: number;
}


export class RequestParams {
  page: number; // Specify the page of result to return Default value: 1 Size range: 1-
  per_page: number; // Specify the number of records to return in one request Default value: 25 Size range: 1-
  sort: string;  // Specify sorting for profiles (currently support only 'date') Default value: date
  after: string;  // Specify last date for vacancy fetching
}
