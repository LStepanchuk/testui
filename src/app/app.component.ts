import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'test-app';
  param = {value: 'world'};

  constructor(translate: TranslateService) {
    translate.addLangs(['en', 'ua']);
    translate.setDefaultLang('en');
    //translate.use('en');
  }
}
