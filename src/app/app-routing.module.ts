import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { LoggedInGuard, AuthGuard } from './Services/route-guard.service';
import { AppComponent } from './app.component';
import { DashboardComponent } from './Views/Dashboard/dashboard.component';
import { MainDashboardComponent } from './Views/Dashboard/Content/main-dashboard/main-dashboard.component';
import { ReportingComponent } from './Views/Dashboard/Content/reporting/reporting.component';
import { SettingsComponent } from './Views/Dashboard/Content/settings/settings.component';

import { AccountComponent } from './Views/Account/account/account.component';
import { SignInComponent } from './Views/Account/sign-in/sign-in.component';

import { ErrorComponent } from './Views/error.component';


const routes: Routes = [
  { path: '', redirectTo: 'account', pathMatch: 'full'},
  { path: 'account',
    component: AccountComponent,
    canActivate: [LoggedInGuard],
    children: [
      { path: '',
        redirectTo: 'sign-in',
        pathMatch: 'full'},
      {
        path: 'sign-in',
        component: SignInComponent
      }
    ]},
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'cameras',
        pathMatch: 'full'
      },
      {
        path: 'cameras',
        component: MainDashboardComponent
      },
      {
        path: 'reporting',
        component: ReportingComponent
      },
      {
        path: 'settings',
        component: SettingsComponent
      }
    ]
  },
  { path: '**', component: ErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: false,
    initialNavigation: true ,
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled',
    enableTracing: false})],
    exports: [RouterModule]
})

export class AppRoutingModule { }



